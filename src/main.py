from flask import Flask, request, render_template
import numpy as np
from tensorflow import keras


x = np.zeros(12)
model = keras.models.load_model("models/Matrix_Filler_NN")

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('main.html')

def processing_params(p_1, p_2, p_3, p_4, p_5, p_6, p_7, p_8, p_9, p_10, p_11, p_12):
    sum = 0
    dictP = {1:p_1, 2:p_2, 3:p_3, 4:p_4, 5:p_5, 6:p_6, 7:p_7, 8:p_8, 9:p_9, 10:p_10, 11:p_11, 12:p_12}
    for i in range(0, 12):
        x[i] = dictP[i+1]
    predict = model.predict(x).flatten()
    res = f"Соотношение матрица-наполнитель = {predict}"
    return res

@app.route('/', methods=['post', 'get'])
def work():
    res = ''
    p_null = False
    if request.method == 'POST':
        dictP = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0}
        for i in range(1, 13):
            dictP[i] = request.form.get('p'+str(i))
            if dictP[i] == '':
                p_null = True
                break
            dictP[i] = float(dictP[i])
        if p_null == False:
            res = processing_params(dictP[1], dictP[2], dictP[3], dictP[4], dictP[5], dictP[6], dictP[7], dictP[8], dictP[9], dictP[10], dictP[11], dictP[12])
        else: res = "Ошибка: Все параметры должны быть указаны!"

    return render_template('main.html', result=res)

if __name__ == '__main__':
    app.run()